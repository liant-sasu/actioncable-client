# ROADMAP

Rapidly decided may the 30rd, 2024.

## v0.1.0

Repackaging of the original project.

- GitLab CI/CD Auto package deployment
- First tests and some coverage/Code Quality reports

## v0.2.0

Integrate device_authorization_grant.

- Should integrate the first steps of authentication of a device-like (no display) application
- Manage refresh_token management
- Report connection errors and server's reasons

## v0.3.0 <-- Here today

Please clean the code of your user:

- Better support of ActionCable spirit of "action" mechanism in an Object-oriented way